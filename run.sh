#!/bin/bash

##################################
#   -- @Name: MobGui build tool
#	-- @Description: Updates version and build numbers and pushes to git repo
#	-- @Author: Marcel Benning
#	-- @Usage: sh run.sh
#	-- @Version: 1.0
#	-- @Web: http://ccmob.net
##################################

bash log.sh "Getting old build number..."
typeset -i buildno=$(cat build)
bash log.sh "Old build number: $buildno"
buildnon=$((buildno+1))
bash log.sh "New build number: $buildnon"

bash log.sh "Replacing old build numbers in initstartup.msx ..."
cat initstartup.msx | sed -e "s/MOBGUI__BUILDNO = $buildno/MOBGUI__BUILDNO = $buildnon/" > initstartup.tmp
mv initstartup.tmp initstartup.msx
cat initstartup.msx | sed -e "s/	-- @Buildnumber: $buildno/	-- @Buildnumber: $buildnon/" > initstartup.tmp
mv initstartup.tmp initstartup.msx
bash log.sh "Done."
bash log.sh "Saving new build number: $buildnon"
echo "$buildnon" > build
bash log.sh "Pushing to bitbucket..."
git add --all *
git add -u
git commit
git push
bash log.sh "Done."
