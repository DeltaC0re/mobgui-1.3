--[[
    -- @Name: Command parser
	-- @Description: 
	-- @Author: Marcel Benning
	-- @Usage: -- none, gets autoloaded from system core
	-- @Version: 1.0
	-- @Web: http://ccmob.net
--]]

-- [[ Command parser ]] --

function __package_command(input)
	local packet = Class("mobgui.parser.packet")
	local parts = split(input, " ")
	packet.cmd = parts[1]
	packet.options = {}
	packet.parameter = {}
	for i = 2, #parts do
		local part = parts[i]
		if startsWith(part, "-") then
			packet.options[#packet.options + 1] = part
		else
			packet.parameter[#packet.parameter + 1] = part
		end
	end
	packet.optionExists = function(key)
		for i = 1, #packet.options do
			if packet.options[i] == key then
				return true	
			end
		end
		return false
	end
	return packet
end

function __mobgui_run(input) --Nano:False
	local cmds = {}
	cmds = split(input, " ")	
	char = string.char(34)
	if #cmds == 1 then
		shell.run(cmds[1])
	else
		if #cmds > 0 then
			cmd = "shell.run(" .. char .. cmds[1] .. char .. ","
			for i = 2 , #cmds do
				cmd = (cmd .. char .. cmds[i] .. char)
				if i == #cmds then
					cmd = cmd .. ")"
				else
					cmd = cmd .. ","
				end
			end
			func = loadstring(cmd)
			setfenv(func, getfenv())
			return func(id)
		end
	end
end

function ioexecuteCommand(input, sv)
	if string.find(input, "&&") then
		table.insert( CMD_HISTORY, input )
		local inputs = split(input, "&&")
		for _,inp in ipairs(inputs) do
			ioparseCommand(inp, true)
		end
		return
	else
		ioparseCommand(input, sv)
	end
end

__CP = {}
__CP.CPHandler = {}

function __CP.registerCommand(command, f)
	local cpHandler = Class("mobgui.commandParserMod.handler")
	cpHandler.cmd = command
	cpHandler.f = f
	__CP.CPHandler[#__CP.CPHandler+1] = cpHandler
end

function ioparseCommand(input, ins) --Nano:True
	if ins ~= nil then else
		table.insert( CMD_HISTORY, input )
	end
	for index, cpHandler in ipairs(__CP.CPHandler) do
		if startsWith(input, cpHandler.cmd .. " ") or input == cpHandler.cmd then
			if type(cpHandler.f) == "function" then
				local packet = __package_command(input)
				cpHandler.f(input, packet)
				return
			end
		end
	end
	return __mobgui_run(input)
end

function __CP.parse_upgrade(input, cmdPacket)
	printDebugInfo("Upgrading MobGui " .. MOBGUI__VERSION .. "[" .. MOBGUI__BUILDNO .. "]")
	downloadProgramms()
	printDebugInfo("Saving stuff ...")
	setExitMsg("update")
	if #cmdPacket.options >= 1 then
		if cmdPacket.options[1] == "-f" or	cmdPacket.options[1] == "-force" then
			setExitMsg("force-upgrade")
		end
	end
    printDebugInfo("Updating core dependencies")
    IS_RUNNING = false
end

function __CP.parse_debug(input)
	setExitMsg("debug")
    IS_RUNNING = false
end

function __CP.parse_exit(input)
	IS_RUNNING = false
end

function __CP.parse_apps(input)
	printDebugInfo("Appsmanager is depricated")
end

function __CP.parse_update(input, cmdPacket)
	if #cmdPacket.parameter >= 1 then
		local iName = cmdPacket.parameter[1]
		ioparseCommand("yaourt " .. iName, false)
	else
		printDebugInfo("update <pkgName")
	end
end

function __CP.parse_runwebprogram(input)
	local __cmds = split(input, " ")
	if #__cmds < 2 then
		printDebugInfo("usage: runWebProgram http://example.com/script or pastebin://pastebinCode")
	else
		local path = __cmds[2]
		if startsWith(path, "pastebin://") then
			local code = string.sub(path, 12, string.len(path))
			shell.run("pastebin", "run", code)
		elseif startsWith(path, "pb://") then
			local code = string.sub(path, 6, string.len(path))
			shell.run("pastebin", "run", code)
		elseif startsWith(path, "https://") or startsWith(path, "http://") then
			if http.downloadFile(path, TMP_PATH .. "run_web_prg.tmp") then
				shell.run(TMP_PATH .. "run_web_prg.tmp")
				fs.delete(TMP_PATH .. "run_web_prg.tmp")
			else
				printDebugError(colors.orange, "404 - Url not found.")
			end
		else
			printDebugError(colors.orange, "Allowed protcols : http(s)://... and pastebin://pastebinCode")
		end
	end
end

function __CP.parse_version()
	printDebugInfo("")
	printDebugInfo("	MobGui version " .. MOBGUI__VERSION .. "[" .. MOBGUI__BUILDNO .. "]")
	printDebugInfo("")
	printDebugInfo("	Web: https://ccmob.net")
	printDebugInfo("	Author: MinecraftM0b")
	printDebugInfo("")
end

-- alias for __CP
CommandParser = {}
CommandParser.registerCommand = __CP.registerCommand

__CP.registerCommand("debug", __CP.parse_debug)
__CP.registerCommand("exit", __CP.parse_exit)
__CP.registerCommand("apps", __CP.parse_apps)
__CP.registerCommand("update", __CP.parse_update)
__CP.registerCommand("upgrade", __CP.parse_upgrade)
__CP.registerCommand("runWebProgram", __CP.parse_runwebprogram)
__CP.registerCommand("version", __CP.parse_version)

executeCommand = ioexecuteCommand