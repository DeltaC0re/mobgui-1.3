#!/bin/bash
#--[[--
#  Author : MinecraftM0b (http://www.ccmob.net)
#  (c) Marcel Benning 2015
#--]]--
bash log.sh "Pushing programs folder to bitbucket..."
git add --all programs/*
git add -u
git commit
git push
bash log.sh "Done."
