<?php 

/*
	Author : MinecraftM0b (http://www.ccmob.net)
 	(c) Marcel Benning 2015
*/

/*

	DO NOT MODIFY !!!

*/

$apis = Array();

class Api{

	function __construct($apiName, $apiVersion, $localPath, $remotePath, $osLoad) {
		$this->apiName = $apiName;
		$this->apiVersion = $apiVersion;
		$this->localPath = $localPath;
		$this->remotePath = $remotePath;
		$this->osLoad = $osLoad;
	}

}

function addApi($apiName, $apiVersion, $localPath, $remotePath, $osLoad)
{
	global $apis;
	array_push($apis, new Api($apiName, $apiVersion, $localPath, $remotePath,$osLoad));
}

/*

	Add apis as you like

*/

addApi(
	"database", 
	"1.0", 
	"/etc/lib/database", 
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/database", 
	true
	);
	
addApi(
	"debug",
	"1.0",
	"/etc/lib/debug",
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/debug",
	true
	);
addApi(
	"drawapi",
	"1.0",
	"/etc/lib/drawapi",
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/drawapi",
	true
	);
addApi(
	"httpapi",
	"1.0",
	"/etc/lib/httpapi",
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/httpapi",
	true
	);
addApi(
	"networkapi",
	"1.0",
	"/etc/lib/networkapi",
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/networkapi",
	true
	);
addApi(
	"termapi",
	"1.0",
	"/etc/lib/temrapi",
	"https://bitbucket.org/DeltaC0re/mobgui-1.3/raw/master/apis/termapi",
	true
	);


echo json_encode($apis);

?>